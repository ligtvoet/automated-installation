**ART-DECOR installation with docker, under development**

Dockerfiles to install ART-DECOR.

*Not tested for production use*
---
## Installation
* Install git
* Clone our repo 'Automated installation'
* Install docker engine and docker compose, see https://docs.docker.com/compose/install/
* Change directory into the ./docker directory, where the docker-compose file is located.
* Build docker images and start the docker container:
  docker-compose down && docker-compose build --no-cache && docker-compose up

* From your local machine, connect to:
  1. nginx frontend: localhost:443/art-decor
  2. eXist-db      : localhost:8877
  3. tomcat        : localhost:8080

* Note that the admin password is pre-generated, shown during the start of the docker containers, and also stored in the ant container in ./ant_src/new.admin.password
  During installation the password is shown as:
ant_1     | set-admin-password:
ant_1     |      [echo] New admin password will be admin: ....

---
Information below will be deprecated once this step is incorporated as a build stage for the tomcat image
## art-decor-war-docker/art-decor-war-create.sh 

Start by creating a art-decor.war that is slightly different then the default ART-DECOR one.
All URLs that point to localhost should point to the URL defined by the docker exist service 'exist'

1. Run art-decor-war-docker/art-decor-war-create.sh 
2. (optional) Check that the art-decor.war is present as art-decor-war-docker/new/art-decor.war 
3. Docker will use this file in the docker image 

---

## Starting docker 

1. cd docker 
2. Starting ART-DECOR: docker-compose up --build

Stopping ART-DECOR:
1. docker-compose down

---

