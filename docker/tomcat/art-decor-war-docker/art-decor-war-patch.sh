#!/bin/bash
# patch art-decor.war with paths to docker service

URL=exist
# PORT=8877
# inside the docker container, exist is running on localhost:8080, exposed as exist:8080 from tomcat
PORT=8080

#      All properties come in two flavors:
#      1. internal use, these are used by Orbeon to directly access the eXist database.
#      2. external use, These are used when links to resources or services are passed to the client.
#    -->

# Original
#  <property as="xs:anyURI" name="art.exist.url" value="http://localhost:8877/art"/>
#  <property as="xs:anyURI" name="art.external.exist.url" value="http://localhost:8877/art"/>
# Patched 
#  <property as="xs:anyURI" name="art.exist.url" value="http://exist:8080/exist/art"/>
#  <property as="xs:anyURI" name="art.external.exist.url" value="http://localhost:8877/exist/art"/>

file=WEB-INF/resources/config/properties-local.xml
# for internal paths
# replace localhost with external, but not on lines containing external.exist
sed -i '/external\.exist/!s/localhost:8877/'"${URL}:${PORT}\/exist"'/g' $file

# for external paths, only contextpath /exist has to be added
# replace external urls, :8877 -> :8877/exist 
# from the docker host, exist is reachable by :8877
sed -i '/external\.exist/s/localhost:8877/localhost:8877\/exist/g' $file

# Original
#    <page id="ada" path="(?!/apps)?/ada(.*)" view="http://localhost:8877/ada${1}"/>
# Patched 
#    <page id="ada" path="(?!/apps)?/ada(.*)" view="http://exist:8080/ada${1}"/>

file=WEB-INF/resources/page-flow.xml
# replace all localhost with URL
# replace all 8877 ports with PORT
sed -i 's/http:\/\/localhost:8877/http:\/\/'"${URL}:${PORT}\/exist"'/g' $file

