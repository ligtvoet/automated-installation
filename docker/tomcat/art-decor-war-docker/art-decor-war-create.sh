#!/bin/bash
# create art-decor.war for use in docker
# what does this script do?
#  - assumes the art-decor.war is present in this folder
#  - update the resources/img folder
#  - art-decor-war-patch.sh -> all paths that point to localhost:8877 towards eXist-db should be paths defined in docker-compose.yml, so exist:8080
#  - result is /art-decor-war-docker/art-decor.war 

# yum install -y svn
# for alpine we need to install subversion with apk
apk add subversion

# get this scriptname
SCRIPT_NAME=$(basename $BASH_SOURCE)
# get the directory where this script is located
SCRIPT_DIR=$(dirname $(readlink -f $0))

# different versions of the orbeon resources folder
# REMOTE_REPO=https://svn.code.sf.net/p/artdecor/code-0/trunk artdecor-code-0
# REMOTE_REPO=https://svn.code.sf.net/p/artdecor/code-0/branches/exist-db-5/orbeon2019/WEB-INF/resources
REMOTE_REPO=https://svn.code.sf.net/p/artdecor/code-0/branches/stable/orbeon2018/WEB-INF/resources

WAR_NAME=art-decor.war
SCRIPT_TEMP_DIR=new

# this part deprecated since we do this in the tomcat Dockerfile
# url where to download the current art-decor.war
# wget -O ${WAR_NAME} https://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fartdecor%2Ffiles%2FOrbeon%2Fart-decor.war%2Fdownload&ts=1585500140
# wait

# first do cleanup
rm -Rf old
rm -Rf ${SCRIPT_TEMP_DIR}
mkdir old
mkdir ${SCRIPT_TEMP_DIR}
cd old
mv ../${WAR_NAME} .
jar -xvf ${WAR_NAME}
cd ..
# copy unpack dirs from the old unpacked war
cp -R ./old/META-INF ${SCRIPT_TEMP_DIR}
cp -R ./old/WEB-INF/ ${SCRIPT_TEMP_DIR}
cd ${SCRIPT_TEMP_DIR}
# do subversion export (export because now we don't get subversion metafiles)
echo
echo ${SCRIPT_NAME}: do subversion export: into temp directory that will be deleted later on
svn export ${REMOTE_REPO} resources

# move resources to proper location
cp -R ./resources/ WEB-INF/
rm -Rf ./resources
echo Do local patches
chmod +x ${SCRIPT_DIR}/art-decor-war-patch.sh
${SCRIPT_DIR}/art-decor-war-patch.sh

# repackage the .war
echo; echo ${SCRIPT_NAME}: repackage the .war
jar -cvfm ${WAR_NAME} ${SCRIPT_DIR}/Manifest.txt ./*
# copy the new art-decor.war to docker
mv ${WAR_NAME} /art-decor-war-docker/

echo done building art-decor.war as: /art-decor-war-docker/art-decor.war
 
